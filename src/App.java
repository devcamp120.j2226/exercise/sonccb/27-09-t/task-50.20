import com.devcamp.j50_javabasic.s10.NewDevcampApp;

public class App {
   public static void main(String[] args) throws Exception {
       // This is a comment
       String appName = "Devcamp will help everyone to know coding.";
       System.out.println("Hello, World! " + appName.length());// This is a comment
       System.out.println("uppercase: " + appName.toUpperCase());// This is a comment
       System.out.println("LOWERCASE: " + appName.toLowerCase());// This is a comment
       NewDevcampApp.name("HieuHN", 42);
       NewDevcampApp newApp = new NewDevcampApp();
       newApp.name("Ha Ngoc Hieu");
   }
}

 